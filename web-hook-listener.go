package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"log"
	"net/http"
	"os"
	"os/exec"
	"strings"
)

type httpResponse map[string]interface{}

type route struct {
	Path    string   `json:"path"`
	Command []string `json:"cmd"`
}

type config struct {
	Routes      []route `json:"routes"`
	Host        string  `json:"host"`
	Certificate string  `json:"ssl-cert-path"`
	Key         string  `json:"ssl-key-path"`
}

func main() {
	// parse console arguments
	configFile := flag.String("c", "./config.json", "Path to the configuration filename")
	flag.Parse()

	// load configuration file
	configuration, err := loadConfiguration(*configFile)
	if err != nil {
		log.Fatalln(err.Error())
	}

	// setup routes
	for _, route := range configuration.Routes {
		http.HandleFunc(route.Path, func(w http.ResponseWriter, r *http.Request) {
			cmd := exec.Command(route.Command[0], route.Command[1:]...)
			encoder := json.NewEncoder(w)
			if response, err := cmd.CombinedOutput(); err != nil {
				encoder.Encode(httpResponse{"status": false, "response": err.Error()})
			} else {
				encoder.Encode(httpResponse{"status": true, "response": strings.Trim(string(response), "\n")})
			}
		})
	}

	// start listening
	if configuration.Certificate != "" && configuration.Key != "" {
		// with TLS
		if err := http.ListenAndServeTLS(configuration.Host, configuration.Certificate, configuration.Key, nil); err != nil {
			fmt.Println(err.Error())
		}
	} else {
		// without TLS
		if err := http.ListenAndServe(configuration.Host, nil); err != nil {
			fmt.Println(err.Error())
		}
	}
}

// load configuration
func loadConfiguration(configPath string) (conf config, err error) {
	file, err := os.Open(configPath)
	if err == nil {
		decoder := json.NewDecoder(file)
		err = decoder.Decode(&conf)
	}
	return
}
